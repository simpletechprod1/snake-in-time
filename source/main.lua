import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"
import "snakePart"

local gfx <const> = playdate.graphics
local snakeHeadSprite = nil
local snakePartSize = 10
local snakeDirection = 'u' -- directions can be u/d/l/r

function myGameSetUp()
    snakeHeadSprite = SnakePart(200, 120, snakePartSize)
    snakeHeadSprite:add()
end

myGameSetUp()

function playdate.update()

    if snakeDirection == 'u' then 
        snakeHeadSprite:moveBy( 0, -2 )
    end
    if snakeDirection == 'd' then 
        snakeHeadSprite:moveBy( 0, 2 )
    end
    if snakeDirection == 'l' then 
        snakeHeadSprite:moveBy( -2, 0 )
    end
    if snakeDirection == 'r' then 
        snakeHeadSprite:moveBy( 2, 0 )
    end

    if playdate.buttonIsPressed( playdate.kButtonUp ) and snakeDirection ~= 'd' then
        snakeDirection = 'u'
    end
    if playdate.buttonIsPressed( playdate.kButtonRight ) and snakeDirection ~= 'l' then
        snakeDirection = 'r'
    end
    if playdate.buttonIsPressed( playdate.kButtonDown ) and snakeDirection ~= 'u' then
        snakeDirection = 'd'
    end
    if playdate.buttonIsPressed( playdate.kButtonLeft ) and snakeDirection ~= 'r' then
        snakeDirection = 'l'
    end

    gfx.sprite.update()
    playdate.timer.updateTimers()

end