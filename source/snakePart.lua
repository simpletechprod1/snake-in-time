local gfx = playdate.graphics

class("SnakePart").extends(gfx.sprite)

function SnakePart:init(x, y, size)
    SnakePart.super.init(self)
    self:moveTo(x, y)
    local snakePartImage = gfx.image.new(size, size)
    gfx.pushContext(snakePartImage)
        gfx.fillRect(0,0, size, size)
    gfx.popContext()
    self:setImage(snakePartImage)
end